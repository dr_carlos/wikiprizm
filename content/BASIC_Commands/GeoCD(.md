---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= GeoPD( = == Description == This command\nreturns\
    \ the cumulative geometric distribution of event x and\nprobability P. == Syntax\
    \ ==\u2019\u2018\u2019GeoCD(\u2019\u2019\u2019\u2018\u2019x\u2019\u2018,\u2019\
    \u2018P\u2019\u2019 x can be either\nvalue\u2026\u2019"
  timestamp: '2012-02-15T12:42:17Z'
title: GeoCD(
---

# GeoPD(

## Description

This command returns the cumulative geometric distribution of event x
and probability P.

## Syntax

**GeoCD(***x*,*P*

x can be either value or list.

## Example

`GeoCD(5,.3`
