---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018== GridOn == == Description == This\ncommand turns\
    \ the graph screen\u2019s grid on. == Syntax == GridOn (no\narguments) == Example\
    \ == GridOn \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-29T20:14:18Z'
title: GridOn
---

## GridOn

## Description

This command turns the graph screen's grid on.

## Syntax

GridOn (no arguments)

## Example

`GridOn`
