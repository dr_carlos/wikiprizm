---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-29T20:09:13Z'
title: CoordOn
---

# CoordOn

## Description

This command makes the coordinates display in the graph screen.

## Syntax

CoordOn (no arguments)

## Example

`CoordOn`
