---
revisions:
- author: Ashbad
  comment: "Created page with \u2018= \u25BAa+bi = == Description == This command\n\
    converts value to simple a+bi complex number format. == Syntax\n==\u2019\u2018\
    Value\u2019\u2019\u2019\u2019\u2018\u25BAa+bi\u2019\u2019\u2019 == Example ==\
    \ 52\u25BAa+bi \\[\\[Category:BASIC_C\u2026\u2019"
  timestamp: '2012-02-15T03:48:55Z'
title: "\u25BAa+bi"
---

# ►a+bi

## Description

This command converts value to simple a+bi complex number format.

## Syntax

*Value***►a+bi**

## Example

`52►a+bi`
