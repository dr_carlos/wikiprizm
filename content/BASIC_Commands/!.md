---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ! = == Description == This command\nreturns\
    \ the factorial of value. == Syntax ==\u2019\u2018Value\u2019\u2019\u2019\u2019\
    \u2018!\u2019\u2019\u2019 ==\nExample == 10! \\[\\[Category:BASIC_Commands\\]\\\
    ]\u2019"
  timestamp: '2012-02-16T23:04:33Z'
title: '!'
---

# !

## Description

This command returns the factorial of value.

## Syntax

*Value***!**

## Example

`10!`
