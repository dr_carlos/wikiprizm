---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= d(Reg) = == Description == This is\nthe\u2019\
    \u2018\u2019d\u2019\u2019\u2019 value used in regression. Can be treated as a\
    \ number.\nRegression including \u2019\u2018\u2019d\u2019\u2019\u2019 must be\
    \ calculated before usage. ==\nSyntax ==\u2026\u2019"
  timestamp: '2012-02-16T00:15:20Z'
title: D(Reg)
---

# d(Reg)

## Description

This is the **d** value used in regression. Can be treated as a number.
Regression including **d** must be calculated before usage.

## Syntax

**d**

## Example

`d`

`d+4`
