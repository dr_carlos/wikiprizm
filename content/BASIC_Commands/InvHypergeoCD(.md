---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= InvHypergeoCD( = == Description == This\ncommand\
    \ returns the cumulative inverse hypergeometric distribution.\n== Syntax ==\u2019\
    \u2018\u2019InvHypergeoCD(\u2019\u2019\u2019\u2018\u2019x\u2019\u2018,\u2019\u2018\
    n\u2019\u2018,\u2019\u2018k\u2019\u2018,\u2019\u2018N\u2019\u2019\u2019\u2019\u2018\
    )\u2019\u2019\u2019 \u2026\u2019"
  timestamp: '2012-02-16T00:53:16Z'
title: InvHypergeoCD(
---

# InvHypergeoCD(

## Description

This command returns the cumulative inverse hypergeometric distribution.

## Syntax

**InvHypergeoCD(***x*,*n*,*k*,*N***)**

## Example

`InvHypergeoCD(7,10,5,100)`
