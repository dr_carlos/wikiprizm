---
title: Red
---

# Red

## Description

This command sets the subsequent Locate or other text command to render
in red.

## Syntax

**Red \<text command>**

## Example

`Red Locate 1, 1, "Asdf`
