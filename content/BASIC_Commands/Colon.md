---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= : = == Description == This is used to\nseparate\
    \ lines. == Syntax ==\u2019\u2018Program_Line\u2019\u2019\u2019\u2019\u2018:\u2019\
    \u2019\u2019\u2018\u2019Another Line\u2019\u2019\n== Example == 3\u2192A:Locate\
    \ 1,1,A \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-24T20:18:49Z'
title: Colon
---

# :

## Description

This is used to separate lines.

## Syntax

*Program_Line***:***Another Line*

## Example

`3→A:Locate 1,1,A`
