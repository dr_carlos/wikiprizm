---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= OnePropZTest = == Description == This\ncommand\
    \ returns the range of data based on the sample probability\nand confidence level.\
    \ == Syntax ==\u2019\u2018\u2019OnePropZTest\u2019\u2019\u2019 \u2019\u2018\\\
    # of\npositive va\u2026\u2019"
  timestamp: '2012-02-22T20:02:20Z'
title: OnePropZTest
---

# OnePropZTest

## Description

This command returns the range of data based on the sample probability
and confidence level.

## Syntax

**OnePropZTest** *\# of positive value*,*\# of samples*,*Confidence
Level*

## Example

`OnePropZTest 30,160,.95`
