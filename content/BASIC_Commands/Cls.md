---
revisions:
- author: Purobaz
  comment: /\* Description \*/
  timestamp: '2012-02-22T22:39:01Z'
title: Cls
---

# Cls

## Description

This command clears the screen. It doesn't remove the ViewWindow.

## Syntax

**Cls**

## Example

`Cls`
