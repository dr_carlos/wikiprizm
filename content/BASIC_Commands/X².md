---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= X\xB2 = == Description == This command\nsquares\
    \ the value. == Syntax ==\u2019\u2018Value\u2019\u2019\u2019\u2018\u2019\xB2\u2019\
    \u2019\u2019 == Example == 3\xB2\n\\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T03:44:00Z'
title: "X\xB2"
---

# X²

## Description

This command squares the value.

## Syntax

*Value***²**

## Example

`3²`
