---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Amt_\u03A3INT( = == Description == This\ncommand\
    \ returns the total principal and interest paid from payment\nPM1 to PM2. == Syntax\n\
    ==\u2019\u2018\u2019Amt_\u03A3INT(\u2019\u2019\u2019\u2018\u2019PM1\u2019\u2018\
    ,\u2019\u2018PM2\u2019\u2018,\u2019\u2018I%\u2019\u2018,\u2019\u2018PV\u2019\u2018\
    ,\u2019\u2018\u2026\u2019"
  timestamp: '2012-02-15T23:37:38Z'
title: "Amt_\u03A3INT("
---

# Amt_ΣINT(

## Description

This command returns the total principal and interest paid from payment
PM1 to PM2.

## Syntax

**Amt_ΣINT(***PM1*,*PM2*,*I%*,*PV*,*PMT*,*P/Y*,*C/Y***)**

## Example

`Amt_ΣINT(12,10,5.5,1000,1000,12,12)`
