---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= CellMedian( = == Description == This\ncommand\
    \ returns the media value in a specified cell range. == Syntax\n==\u2019\u2018\
    \u2019CellMedian(\u2019\u2019\u2019\u2018\u2019start_cell\u2019\u2018:\u2019\u2018\
    end_cell\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019 == Example\n==\u2026\
    \u2019"
  timestamp: '2012-02-16T00:04:50Z'
title: CellMedian(
---

# CellMedian(

## Description

This command returns the media value in a specified cell range.

## Syntax

**CellMedian(***start_cell*:*end_cell***)**

## Example

`CellMedian(A3:C5)`
