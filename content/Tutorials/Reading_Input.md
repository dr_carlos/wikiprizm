---
revisions:
- author: Gbl08ma
  timestamp: '2012-10-05T17:55:47Z'
title: Reading Input
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

Getting input from the user makes an addin is not only useful, it is
also required for proper addin functionality. There are many functions
that can read input, each with their advantages and disadvantages.

## GetKey

[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) is a very basic way
to read key input. This function is blocking (stops execution until a
user presses a key that is not \[MENU\]). It also handles the \[MENU\]
key.

## PRGM_GetKey

This is a community made function that calls
[PRGM_GetKey_OS]({{< ref "Syscalls/Keyboard/PRGM_GetKey_OS.md" >}}) and
demangles the key value that
[PRGM_GetKey_OS]({{< ref "Syscalls/Keyboard/PRGM_GetKey_OS.md" >}})
returns. **Note:**
[PRGM_GetKey]({{< ref "Useful_Routines/PRGM_GetKey.md" >}}) function is
not part of the PrizmSDK. You can get the source of the function on the
[PRGM_GetKey]({{< ref "Useful_Routines/PRGM_GetKey.md" >}}) page.

This function is very useful for games because this function doesn't
block and the value returned is easy to understand. (fixme: does this
check for MENU? Add in the text about the value and how it relates to
the keyboard.)

## GetKeyWait_OS

Todo.

## Directly Reading the Keyboard Hardware {#directly_reading_the_keyboard_hardware}

Todo.

## String Input - Easy {#string_input___easy}

For some cases, you will need to grab some textual input from the user.
The OS would be truly silly if it couldn't even get text input. The
easiest form of input is string input. With string input, you can get
any textual input without having to filter or replace keys. To do this,
you must use two syscalls,
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
and
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}}).
The first syscall draws your string buffer with the given settings and
places a cursor in the correct spot.
[EditMBStringCtrl]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringCtrl.md" >}})
will take any keypress and handle it for you. This lets you have a
standardized input method with minimal coding.

First, you must set up the proper variables that are used to store
values and data for the input methods.

    char *buffer = (char*)malloc(256); // Some string length
    int start = 0; // Used for scrolling left and right
    int cursor = 0; // Cursor position
    buffer[0] = '\0'; // This sets the first character to \0, also represented by "", an empty string

That's all the setup you need! Now, we just need to draw the string
before getting keys

[`DisplayMBString`]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})`((unsigned char*)buffer, start, cursor, 1, 1); // Last to parameters are X,Y coords (not in pixels)`

This call draws the string. It will draw characters until it hits the
right side of the screen. Note that this is the only time required to
call this syscall. This is because the next syscall to get called will
be GetKey, a blocking syscall. If this call is omitted, then the screen
will not have anything drawn until you press a key.

Now that it draws, the event loop can be made!

    int key;
    while(1)
    {
      GetKey(&key); // Blocking is GOOD.  This gets standard keys processed and, possibly, powers down the CPU while waiting
      if(key == KEY_CTRL_EXE || key == KEY_CTRL_EXIT)
        break;
      // To add
    }

Now, as you can see, this just polls for keys and breaks out when EXIT
or EXE are pressed - You can change this if you want, but this is
standard procedure for the OS. If you want to check if the user pressed
EXE, just see if key == KEY_CTRL_EXE after the while loop, or handle the
key checking in the while loop.

Next, we need to add in the magic code that handles all key presses.
Replace the *// To add* comment with the following code:

    if(key && key < 30000)
    {
      cursor = EditMBStringChar((unsigned char*)buffer, 256, cursor, key);
      DisplayMBString((unsigned char*)buffer, start, cursor, 1,1);
    }
    else
    {
      EditMBStringCtrl((unsigned char*)buffer, 256, &start, &cursor, &key, 1, 1);
    }

Wait, that's all? Indeed. Since you use the high-level
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) syscall, you can
have the OS handle editing of your string and change the buffer, cursor,
and scrolling! Firstly, you need an if statement because the OS
separates ctrl and char key handling. Control keys start at 30000,
therefore you must handle them separately. Here, we just pass along the
key value to the correct function depending on the type.

The final code is below:

    char *buffer = malloc(256); // Some string length
    int start = 0; // Used for scrolling left and right
    int cursor = 0; // Cursor position
    buffer[0] = '\0'; // This sets the first character to \0, also represented by "", an empty string
    DisplayMBString((unsigned char*)buffer, start, cursor, 1, 1); // Last to parameters are X,Y coords (not in pixels)
    int key;
    while(1)
    {
      GetKey(&key); // Blocking is GOOD.  This gets standard keys processed and, possibly, powers down the CPU while waiting
      if(key == KEY_CTRL_EXE)
      {
        // Ok
        break;
      } 
      else if(key == KEY_CTRL_EXIT)
      {
        // Aborted
        break;
      }
      if(key && key < 30000)
      {
        cursor = EditMBStringChar((unsigned char*)buffer, 256, cursor, key);
        DisplayMBString((unsigned char*)buffer, start, cursor, 1,1);
      }
      else
      {
        EditMBStringCtrl((unsigned char*)buffer, 256, &start, &cursor, &key, 1, 1);
      }
    }

This probably belongs somewhere else, but it should be noted anyway.

For the setup item 0x14, the following flags represent:

0x01: Shift

0x02: Clip

0x04: Alpha (not lock)

0x08: Lowercase Alpha (not lock)

0x84: Alpha Lock

0x88: Lowercase Alpha Lock
