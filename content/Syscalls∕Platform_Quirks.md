---
revisions:
- author: Gbl08ma
  comment: 'Gbl08ma moved page \[\[Syscall/Platform Quirks\]\] to

    \[\[Syscalls/Platform Quirks\]\]: Title consistency'
  timestamp: '2014-11-18T18:11:45Z'
title: "Syscalls\u2215Platform_Quirks"
---

## Syscall Quirks {#syscall_quirks}

-   [realloc](realloc) is not POSIX-compliant, namely, it appears to not
    preserve existing memory when asked to expand a previously-allocated
    chunk. It may also not do an in-place resize.
-   Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
    [timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM
    ERRORs and other undefined behavior, especially with functions that
    change data on the file system. Make sure to stop and uninstall all
    timers before using Bfile functions (and optionally restore them
    later). See
    [Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
    for more information.
-   The OS supports up to 16 simultaneously open files and file finding
    handles, but strange behavior (most likely a OS bug) occurs if files
    other than the one in the first handle slot are located in
    directories (other than the root). This is why the [file copying
    function of the Utilities
    add-in](https://github.com/gbl08ma/utilities/blob/32d0e0d379a7ddb93501a22eff6340b3f5fb7b96/src/fileProvider.cpp#L256),
    which opens both the source and the destination file at once, uses a
    temporary file in the filesystem root as the destination file, and
    only [moves]({{< ref "Syscalls/Bfile/Bfile_RenameEntry.md" >}}) it
    to the intended correct destination path in the end.

## Platform Quirks {#platform_quirks}

-   Your C programs should never explicitly exit(). Users should instead
    be able to leave your Add-In via the \[MENU\] key, like native
    Add-Ins.

The heap implementation on the Prizm appears to have some bugs; it also
appears to not be able to optimize heap allocation so that fragmentation
is avoided. See [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539) for more
information and discussion.

The [stack]({{< ref "OS_Information/Processes.md#stack" >}}) can provide a
bigger continuous RAM area than the heap (up to about 500 KiB, depending on
static RAM requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available to
add-ins, one has to use non-standard (*"incorrect"*) practices such as
preferably using the stack instead of the heap.
