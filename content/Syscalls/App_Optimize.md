---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:12:39Z'
title: App_Optimize
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1630\
**Function signature:** void App_Optimize(void);

Starts the filesystem optimization operation while displaying a
ProgressBar on screen, as if F5 had been pressed in the Memory menu.

## Comments

Returns when the optimization option is finished or when the user aborts
by repeatedly and alternately pressing EXIT and AC/ON.

Calling this from add-ins causes problems, because the optimization
moves things around in the storage memory, causing the add-in code to
change location. This often results in a SYSTEM ERROR when the syscall
returns, or in other undefined behavior.
