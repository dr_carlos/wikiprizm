---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = APP_Program \\| index =\n\
    0x1945 \\| signature = void APP_Program(int, int); \\| header =\nfxcg/app.h \\\
    | synopsis = Opens the built-in\u201DProgram\u201D app. \\|\nparameters = T\u2026\
    \u201D"
  timestamp: '2014-11-18T22:32:00Z'
title: APP_Program
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1945\
**Function signature:** void APP_Program(int, int);

Opens the built-in "Program" app.

## Parameters

The meaning of the parameters is yet to be known. The OS calls this
function with the first parameter set to 1 and the second set to 0.

## Comments

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
