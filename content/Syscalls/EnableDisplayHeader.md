---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-15T15:58:28Z'
title: EnableDisplayHeader
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D7F\
**Function signature:** void EnableDisplayHeader(int action, int value)

If action is 0, GetKey will not automatically show the status area. If action
is 2 and value is not 0, GetKey will show the status area automatically.

## Parameters

-   **action** If action == 0 clear pointer GetKeyShowStatusArea. If action == 2 set
    pointer GetKeyShowStatusArea to value. If action is any other value return
    value at GetKeyShowStatusArea unmodified.
-   **value** Used only if action == 2

## Returns

Whether or not GetKey should show the status area. This may be modified
depending on the value of action.

## Comments

Different versions of the OS use a different point (GetKeyShowStatusArea),
so this syscall should be used instead of manually setting the pointer.

OS 3.60 uses 0xFD802148 as the address of GetKeyShowStatusArea.

    ! If r4==0 clear GetKeyShowStatusArea
    ! If r4==2 set GetKeyShowStatusArea to r5
    ! Other values of r4 will cause the function to simply return the contents of GetKeyShowStatusArea

    EnableDisplayHeader:
            tst r4, r4
            bf  r4NonZero   ! Branch if r4 is non-zero
            mov.l   #GetKeyShowStatusArea, r4 ! If r4 is zero set r4 to GetKeyShowStatusArea
            mov #0, r2
            bra EnableDisplayHeader_exit
            mov.l   r2, @r4     ! Before branching clear the contents of GetKeyShowStatusArea
    ! ---------------------------------------------------------------------------

    r4NonZero:
            mov r4, r0
            cmp/eq  #2, r0
            bf  EnableDisplayHeader_exit ! Branch if r4!=2 without delay
            mov.l   #GetKeyShowStatusArea, r4
            mov.l   r5, @r4

    EnableDisplayHeader_exit:
            mov.l   #GetKeyShowStatusArea, r4
            rts
            mov.l   @r4, r0     ! Return the contents of GetKeyShowStatusArea
    ! End of function EnableDisplayHeader
