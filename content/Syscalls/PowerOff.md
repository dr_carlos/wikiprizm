---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T16:53:40Z'
title: PowerOff
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1839\
**Function signature:** void PowerOff( int displayLogo )

Properly powers the calculator off, optionally showing the Casio logo
and owner information.

## Parameters

-   *int* **displayLogo** - Set to 1 to display Casio logo, set to 0 to
    not display (almost instant power off, but current state is still
    saved).

## Comments

The OS always calls this syscall with **displayLogo** set to 1, except
when the [battery]({{< ref "Syscalls/GetMainBatteryVoltage.md" >}})
level is so low that powering off quickly is important, that displaying
the logo and waiting some seconds would just waste more battery.

It is not certain that some special behavior exists for when
**displayLogo** is set to zero, besides not showing the logo. The system
could set some flag that leads to unexpected behavior, or even change
something lower-level such as the voltage or speed of the CPU or its
peripherals (remember, supposedly the parameter is only zero when the
battery is very low). Even if such behavior doesn't exist in the current
OS versions, it could exist in a future version. If you want to be on
the safe side, always set the parameter to 1.
