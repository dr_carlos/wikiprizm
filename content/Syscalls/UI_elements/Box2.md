---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:00Z'
title: Box2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x17FC\
**Function signature:** void Box2(int height, int unknown)

Creates a box, centered vertically on the screen, with a 3D blue
border.

## Parameters

-   **height** - Height (in text lines) of the box, must be between 0
    and 13, exclusively. If the value is above 7, the corners of the box
    will be beveled.
-   **unknown** - meaning unknown, doesn't seem to do anything.
