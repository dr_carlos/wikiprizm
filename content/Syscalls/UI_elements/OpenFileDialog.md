---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:05Z'
title: OpenFileDialog
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x17E9\
**Function signature:** int OpenFileDialog(unsigned short keycode,
unsigned short\* filenamebuffer, int filenamebufferlength)

Shows a dialog allowing to select a g3p file, the same shown on the
Geometry add-in when opening a g3p file.

## Parameters

-   **keycode** - Meaning unknown, set to 0x756B.
-   **filenamebuffer** - Pointer to buffer that will receive a 16 bit
    string containing the path of the file the user chose. Should be at
    least 0x214 bytes long.
-   **filenamebufferlength** - Size of the buffer needed for the
    previous parameter.

## Returns

The meaning of the return value is unknown.
