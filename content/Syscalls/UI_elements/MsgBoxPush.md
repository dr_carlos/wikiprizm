---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T21:57:12Z'
title: MsgBoxPush
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x17F7\
**Function signature:** void MsgBoxPush(int lines)

Displays an empty message box centered on the screen and with a blue
border.

## Parameters

*int* **lines** - Height of the box in lines (between 1 and 6
inclusive)

## Comments

Text or other content must be drawn using methods not provided by this
syscall, such as [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}). After
using MsgBoxPush, you must call
[MsgBoxPop]({{< ref "Syscalls/UI_elements/MsgBoxPop.md" >}}) in order to
release the resources allocated by the former.
