---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:00:59Z'
title: AUX_DisplayErrorMessage
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0C01\
**Function signature:** void AUX_DisplayErrorMessage(int msgno)

Displays one of the built-in error messages of the OS. The message is
shown inside a message box, with the "Press:\[EXIT\]" message, and the
keyboard input is handled by the syscall, which returns when the user
closes the message box. The message is shown in the currently set system
language.

## Parameters

-   **msgno** - number of the message to show. See a list of messages,
    in English, below.

| msgno         | Message Shown (English, "fx-CG10/20 Manager" emulator w/ OS 1.02)       |
|---------------|-------------------------------------------------------------------------|
| 1             | "Break"                                                                 |
| 2             | "Syntax ERROR"                                                          |
| 3             | "Ma ERROR"                                                              |
| 4             | "Memory ERROR"                                                          |
| 5             | "Go ERROR"                                                              |
| 6             | "Nesting ERROR"                                                         |
| 7             | "Stack ERROR"                                                           |
| 8             | "Argument ERROR"                                                        |
| 9             | "Dimension ERROR"                                                       |
| 10            | "Com ERROR"                                                             |
| 11            | "Transmit ERROR"                                                        |
| 12            | "Receive ERROR"                                                         |
| 13            | "Memory Full"                                                           |
| 14            | "Undefined"                                                             |
| 15            | "Overflow ERROR"                                                        |
| 16            | "Out of Domain"                                                         |
| 17            | "Non-Real ERROR"                                                        |
| 18            | "No Solution"                                                           |
| 19            | "Mismatch"                                                              |
| 20            | "No Variable"                                                           |
| 21            | "Not Found"                                                             |
| 22            | "Application ERROR"                                                     |
| 23            | "System ERROR"                                                          |
| 24            | "Already Exists"                                                        |
| 25            | "Complex Number", "In List"                                             |
| 26            | "Complex Number", "In Matrix"                                           |
| 27            | "Can't Solve!", "Adjust initial", "value or bounds.", "Then try again." |
| 28            | "Range ERROR"                                                           |
| 29            | "Time Out"                                                              |
| 30            | "Condition ERROR"                                                       |
| 31            | "Syntax ERROR"                                                          |
| 32            | "Syntax ERROR"                                                          |
| 33            | "Range ERROR"                                                           |
| 34            | "Circular ERROR"                                                        |
| 35            | "No Real Roots"                                                         |
| 36            | "Version ERROR"                                                         |
| 37            | "Card ERROR"                                                            |
| 38            | "Undefinition", "Error", "Error No\[38\]"                               |
| 39            | "Invalid Card"                                                          |
| 40            | "No Card"                                                               |
| 41            | "SD Card Full"                                                          |
| 42            | "Storage Memory", "Full"                                                |
| 43            | "Data ERROR"                                                            |
| 44            | "Invalid file name", "or folder name."                                  |
| 45            | "Data is protected"                                                     |
| 46            | "Too Many Data"                                                         |
| 47            | "Undefinition", "Error", "Error No\[47\]"                               |
| 48            | "Conversion ERROR"                                                      |
| 49            | "Can't Simplify"                                                        |
| 50            | "Invalid Data Size"                                                     |
| 51            | "Invalid", "Data Number"                                                |
| 52            | "Complex Number", "In Data"                                             |
| 53            | "Undefinition", "Error", "Error No\[53\]"                               |
| 54            | "Too Many", "Variables"                                                 |
| 55            | "Expression", "in use"                                                  |
| 56            | "Undefinition", "Error", "Error No\[56\]"                               |
| 57            | "No Memo"                                                               |
| 58            | "USB Connect", "ERROR"                                                  |
| 59            | "Only one memo", "allowed", "per line."                                 |
| 60            | "CSV error", "in row", "column"                                         |
| 61            | "Undefinition", "Error", "Error No\[61\]"                               |
| 62            | "Invalid Type"                                                          |
| 63            | "Invalid list", "or matrix"                                             |
| 64            | "Input value", "must be integer"                                        |
| 65            | "Input value", "must be matrix"                                         |
| 66            | "Input value", "must be list"                                           |
| 67            | "Input value", "must be", "real number"                                 |
| 68            | "Invalid Polar", "Form"                                                 |
| 69            | "Invalid Setting"                                                       |
| 70            | "Infinitely", "Many Solutions"                                          |
| 71            | "No item", "is selected"                                                |
| 72            | "No File"                                                               |
| 73            | "Too many", "path levels"                                               |
| 74            | "Finish Plotting"                                                       |
| 75            | "Out of Range"                                                          |
| 76            | "Wrong", "argument size", "relationship"                                |
| 77            | "No Data"                                                               |
| 78            | "Improper Number", "of Elements"                                        |
| 79            | "Not Enough", "Elements"                                                |
| 80            | "Underflow"                                                             |
| 81            | "Draw at least", "two points"                                           |
| 82            | "Create filled-in", "figure"                                            |
| 83            | "Requires", "one variable", "expression."                               |
| 84            | "Value outside of", "V-Window range", "can't be updated."               |
| 85            | "Invalid", "Graph Type"                                                 |
| 86            | "Too Many Sectors"                                                      |
| 87            | "Data is in use"                                                        |
| 88            | "Undefinition", "Error", "Error No\[88\]"                               |
| 89            | "Complex Number", "In Vector"                                           |
| 90            | "Complex Number", "In Matrix or", "Vector"                              |
| 91            | "Invalid list,", "matrix or vector"                                     |
| 92            | "Input value", "must be a vector"                                       |
| 93            | "Input value", "must be a matrix", "or vector"                          |
| 94            | ""                                                                      |
| **msgno**\>95 | "Undefinition", "Error", "Error No\[**msgno**\]"                        |
|               |                                                                         |
