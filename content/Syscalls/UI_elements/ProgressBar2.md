---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:46Z'
title: ProgressBar2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1809\
**Function signature:** void ProgressBar2(unsigned char \*heading, int
current, int max)

Displays a progress bar on a message box, with a custom heading. Similar
to [ProgressBar]({{< ref "Syscalls/UI_elements/ProgressBar.md" >}}).

## Parameters

-   **heading** - a pointer to a string with the custom text.
-   **current** - the current value (call with this value set to zero
    for initialization).
-   **max** - the maximum value **current** is expected to take.

## Comments

As with
[ProgressBar0]({{< ref "Syscalls/UI_elements/ProgressBar0.md" >}}) and
[ProgressBar]({{< ref "Syscalls/UI_elements/ProgressBar.md" >}}), this
syscall should be called with **current** as zero for initialization,
and since it calls
[MsgBoxPush]({{< ref "Syscalls/UI_elements/MsgBoxPush.md" >}}), the
message box must be closed with
[MsgBoxPop]({{< ref "Syscalls/UI_elements/MsgBoxPop.md" >}}) in order to
free resources.
