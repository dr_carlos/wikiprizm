---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-04T12:07:29Z'
title: EditMBStringCtrl
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x120E\
**Function signature:** void EditMBStringCtrl(unsigned char\* MB_string,
int posmax, int\* start, int\* xpos, int\* key, int x, int y)

Edits a [multi-byte string]({{< ref "Multi-byte_strings.md" >}})
according to the specified control key (where the GetKey code is higher
or equal to 30000), updates the cursor position and displays the result
at the specified position. To be used in conjunction with
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}}).

## Parameters

-   **MB_string** - pointer to multi-byte string to edit. Must not be
    constant, as this syscall may actually want to modify the string
    contents, for example, when the user presses DEL, cuts or pastes
    texts (cut and paste operations are linked to control keys).
-   **posmax** - maximum size, in bytes, the string can take; the size
    of the buffer **MB_string** points to should be taken into account,
    to avoid buffer overflows (when pasting from the clipboard, for
    example).
-   **start** - pointer to integer that will receive the new display
    start position (see the
    [DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
    parameter with the same name).
-   **xpos** - pointer to integer that will receive the new cursor
    position (see the
    [DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}})
    **cursor** parameter).
-   **key** - pointer to integer with the
    [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) code of the key
    pressed. The value of **key** might be changed, possibly for
    communication between text editing syscalls and GetKey.
-   **x** - horizontal "homescreen" coordinate (like
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})) of the current text
    editing field.
-   **y** - vertical coordinate, in pixels, of the current text editing
    field.

**x** and **y** should be set to the same values used with
[DisplayMBString]({{< ref "Syscalls/Text_and_expression_editing/DisplayMBString.md" >}}),
for graphical consistency.

## Comments

This syscall is for when a control key is pressed during text editing.
If the key pressed is not a control key, or in other words, if the
GetKey code is lower than 30000, use
[EditMBStringChar]({{< ref "Syscalls/Text_and_expression_editing/EditMBStringChar.md" >}})
instead.
