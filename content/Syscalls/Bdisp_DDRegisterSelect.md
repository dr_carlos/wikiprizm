---
revisions:
- author: Dr-carlos
  comment: Replace P1 with registerno
  timestamp: '2022-03-27T05:47:26Z'
title: Bdisp_DDRegisterSelect
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01A2\
**Function signature:** void Bdisp_DDRegisterSelect(int registerno)

Probably responsible for setting a register on the LCD controller
(0xB4000000).

## Comments

This syscall clears bit 4 of 0xA405013C.b, writes registerno (r4) to
0xB4000000 and finally sets bit 4 of 0xA405013C.b again. It looks as if
bit 4 controls the LCD-driver's RS-bit; refer to the R61509-manual for
more information.

After every write to 0xA405013C or 0xB4000000, the SH-4A processor
instruction SYNCO is performed. You can see this in action by reading
the disassembly.

    Bdisp_DDRegisterSelect:

    var_2       = -2

            add #-4, r15
            mov.l   #0xA405013C, r3
            mov #~16, r5
            mov r4, r0
            mov.w   r0, @(4+var_2,r15)
            mov.b   @r3, r1     ! Read a byte from the address 0xA405013C and store that byte into R1
            and r5, r1      ! Clear bit four of R1
            mov.b   r1, @r3
            synco
            mov #0xFFFFFFB4, r6
            shll8   r6
            shll16  r6      ! R6 = 0xB4000000
            mov.w   r4, @r6
            synco
            mov.b   @r3, r0
            or  #16, r0
            mov.b   r0, @r3
            synco
            rts
            add #4, r15
    ! End of function Bdisp_DDRegisterSelect
