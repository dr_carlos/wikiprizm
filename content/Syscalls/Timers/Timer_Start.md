---
revisions:
- author: Ahelper
  comment: Added some return values.
  timestamp: '2014-11-23T04:38:14Z'
title: Timer_Start
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x08DB\
**Function signature:** int Timer_Start(int InternalTimerID)

Starts a [timer]({{< ref "/OS_Information/Timers.md" >}})
[installed]({{< ref "Syscalls/Timers/Timer_Install.md" >}}) at the given
slot.

## Parameters

-   **InternalTimerID** - slot of the timer to start.

## Returns

Returns 0 if the timer was uninstalled, -2 if **InternalTimerID** wasn't
installed.
