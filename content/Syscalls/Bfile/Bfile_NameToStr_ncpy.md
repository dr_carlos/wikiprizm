---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T11:42:26Z'
title: Bfile_NameToStr_ncpy
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DDB\
**Function signature:** void Bfile_NameToStr_ncpy(unsigned char\* dest,
const unsigned short\* source, int n)

Converts a 16 bit string (array of `unsigned short`), suitable for use
with [Bfile syscalls]({{< ref "Syscalls/Bfile/" >}}), to a 8 bit string
(array of `unsigned char`).

## Parameters

-   *unsigned char\** **dest** - Pointer to a buffer that will hold the
    resulting 8 bit string.
-   *const unsigned short\** **source** - Pointer to a 16 bit string
    containing the path to convert.
-   *int* **n** - How many bytes to convert (length of the source
    string, see [#Comments](#Comments) for information on the behavior
    of the syscall wen the length doesn't match **n**).

## Comments

If the length of **source** is below **n**, **dest** is padded. If
**source** is terminated by 0xFFFF, **dest** is padded with 0xFF,
otherwise it is padded with 0x00.

[Bfile_StrToName_ncpy]({{< ref "Syscalls/Bfile/Bfile_StrToName_ncpy.md" >}})
does the opposite of this syscall.
