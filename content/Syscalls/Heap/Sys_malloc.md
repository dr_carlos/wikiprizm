---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T19:57:33Z'
title: Sys_malloc
---

## Synopsis

**Header:** fxcg/heap.h\
**Syscall index:** 0x1F44\
**Function signature:** void \*sys_malloc(int sz)

Allocates a memory block on the heap.

## Parameters

-   **sz** - amount of bytes to allocate.

## Returns

A pointer to the allocated heap memory block, or a null pointer if the
block can't be allocated for some reason.

## Comments

The heap implementation on the Prizm appears to have some bugs; it also
appears to not be able to optimize heap allocation so that fragmentation
is avoided. See [this forum
thread](http://www.cemetech.net/forum/viewtopic.php?t=7539) for more
information and discussion.

The [stack]({{< ref "/OS_Information/Processes.md#stack" >}}) can provide a bigger continuous RAM
area than the heap (up to about 500 KiB, depending on static RAM
requirements, versus about 128 KiB). This, plus the limitations
described above, means that to get the most out of the memory available
to add-ins, one has to use non-standard (*"incorrect"*) practices such
as preferably using the stack instead of the heap.
