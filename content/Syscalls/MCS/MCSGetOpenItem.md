---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = MCSGetOpenItem \\|\nheader\
    \ = fxcg/file.h \\| index = 0x1560 \\| signature = int\nMCSGetOpenItem(unsigned\
    \ char\\* item) \\| synopsis = Gets the name of\nthe currently sel\u2026\u201D"
  timestamp: '2014-07-30T14:19:50Z'
title: MCSGetOpenItem
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1560\
**Function signature:** int MCSGetOpenItem(unsigned char\* item)

Gets the name of the currently selected (using
[MCSGetDlen2]({{< ref "Syscalls/MCS/MCSGetDlen2.md" >}})) MCS item.

## Parameters

-   **item** - Pointer to buffer that will receive the name of the
    item.

## Returns

0 on success and other values on failure.
