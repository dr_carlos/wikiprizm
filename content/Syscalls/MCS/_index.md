---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:34:41Z'
title: MCS
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) which perform
operations on the [Main Memory]({{< ref "File_System.md" >}}).
