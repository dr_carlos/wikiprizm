---
revisions:
- author: Dr-carlos
  comment: Serial_ReadSingle is now included in libfxcg
  timestamp: '2022-03-26T05:36:58Z'
title: Serial_ReadSingle
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BB9\
**Function signature:** int Serial_ReadSingle(unsigned char\* out)

Fetches a single byte from the serial input buffer.

## Parameters

-   **out** - Pointer to `unsigned char` that will receive the fetched
    byte.

## Returns

-   0 if successful,
-   1 if the receive buffer is empty,
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
