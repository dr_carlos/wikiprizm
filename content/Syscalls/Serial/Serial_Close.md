---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:23:10Z'
title: Serial_Close
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BB8\
**Function signature:** int Serial_Close(int mode)

Closes the serial port, clearing all buffers and optionally aborting any
active transmissions.

## Parameters

-   **mode**: Set to 1 to abort any active transmissions, anything else
    to fail if any transmission is pending.

## Returns

0 on success, or 5 if the port was not closed because a transmission is
pending. 5 will only be returned if **mode** is not passed as 1.
