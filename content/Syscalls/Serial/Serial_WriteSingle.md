---
revisions:
- author: Dr-carlos
  comment: Serial_WriteSingle is included in libfxcg
  timestamp: '2022-03-26T05:38:30Z'
title: Serial_WriteSingle
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBC\
**Function signature:** int Serial_WriteSingle(unsigned char x)

Sends a single byte through the 3-pin serial port, by putting it in the
transmit buffer.

## Parameters

-   **x** - byte to transmit.

## Returns

-   0 if successful,
-   2 if no space is available in the serial transmit buffer (which has
    a size of 256 bytes),
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
