---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T23:00:15Z'
title: RTC_GetTime
---

## Synopsis

**Header:** fxcg/rtc.h\
**Syscall index:** 0x02C0\
**Function signature:** void RTC_GetTime(unsigned int\* hour, unsigned
int\* minute, unsigned int\* second, unsigned int\* millisecond)

Gets the Real-Time Clock time.

## Parameters

-   **hour** - pointer to integer that will receive the current hour.
-   **minute** - pointer to integer that will receive the current
    minute.
-   **second** - pointer to integer that will receive the current
    second.
-   **millisecond** - pointer to integer that will receive the current
    millisecond. This is as exact as the [1/128 second base
    tick]({{< ref "Syscalls/RTC/RTC_GetTicks.md" >}}) allows.
