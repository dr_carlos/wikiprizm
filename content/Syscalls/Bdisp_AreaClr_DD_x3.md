---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_AreaClr_DD_x3 \\|\n\
    index = 0x01B6 \\| signature = void Bdisp_AreaClr_DD_x3(void\\* p1) \\|\nheader\
    \ = fxcg/display.h \\| parameters = The meaning of the\nparameter\u2026\u201D"
  timestamp: '2014-07-29T11:11:31Z'
title: Bdisp_AreaClr_DD_x3
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01B6\
**Function signature:** void Bdisp_AreaClr_DD_x3(void\* p1)

The function of this syscall is unknown.

## Parameters

The meaning of the parameter is unknown.
