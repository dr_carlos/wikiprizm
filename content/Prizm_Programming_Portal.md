---
revisions:
- author: Dr-carlos
  comment: Minor rewording to include Casio fx-CG 50.
  timestamp: '2021-10-07T19:51:42Z'
title: Prizm_Programming_Portal
---

Welcome to the Prizm Programming Portal! If you got here, it's probably
because you have some interest in programming your Casio fx-CG 10/20/50,
or in its internals. If you have questions, or if you get lost in this
Wiki, don't let that interest vanish: say something on the [Cemetech
Prizm forums](http://www.cemetech.net/forum/viewforum.php?f=68) and
we'll do our best to help you. Similarly, if you want to contribute to
this documentation, or if you have suggestions, you should also post to
the forums in [this
topic](http://www.cemetech.net/forum/viewtopic.php?t=8022).

## Development Methods and Tools {#development_methods_and_tools}

The Casio Prizm can be programmed using at least three different
methods, which allow for creating programs with varying degrees of
freedom and computing power. More information can be found on the aptly
named article
[Development_Methods_and_Tools]({{< ref "Development_Methods_and_Tools.md" >}}).

If you are interested in add-in development alone, please read on [how
to setup PrizmSDK]({{< ref "Tutorials/PrizmSDK_Setup_Guide.md" >}}) and
be aware that unless otherwise noted, information in this Wiki,
especially the syscall names, are relative to the PrizmSDK and libfxcg.

## Reference Guide {#reference_guide}

-   [Technical Info]({{< ref "Technical_Documentation/" >}}) - Documentation of
    the Prizm's hardware and other system features.
-   [LuaZM_Reference]({{< ref "LuaZM_Reference.md" >}}) - A budding
    guide to the LuaZM Lua interpreter for the Casio Prizm.
-   [BASIC Reference]({{< ref "BASIC_Commands/" >}}) - Contains
    documentation on Prizm-Basic functions.
-   [Secret_Key_Combinations]({{< ref "Secret_Key_Combinations.md" >}}) -
    Access the Test Mode, Diagnostic Mode, boot code menus, and more.
-   [Product_ID_Information]({{< ref "Product_ID_Information.md" >}}) -
    Know the meaning of the code on the sticker on the back.

**Specific to add-in development:**

-   [Addin_Usability_Guidelines]({{< ref "Addin_Usability_Guidelines.md" >}}) -
    A list of guidelines to make your add-in software more
    user-friendly.
-   [Optimization_Tips]({{< ref "Optimization_Tips.md" >}}) - Tips on
    making your programs faster and more efficient.
-   [Useful_Routines]({{< ref "Useful_Routines.md" >}}) - A list of
    useful routines that can be used in your programs.

### Native OS reference {#native_os_reference}

-   [CASIOWIN]({{< ref "OS_Information/" >}}) - General information on the
    OS.
-   [G3A_File_Format]({{< ref "G3A_File_Format.md" >}}) - A description
    of the file format for add-ins.
-   [System Calls]({{< ref "Syscalls/" >}}) - Contains documentation of
    functions for use in add-ins. See the calling convention
    [here]({{< ref "Syscalls.md" >}}).
    -   [Syscalls∕Platform_Quirks]({{< ref "Syscalls∕Platform_Quirks.md" >}}) -
        Various caveats of the system calls and the Prizm platform in
        general for programmers.
-   [Keycode_Reference]({{< ref "Keycode_Reference.md" >}}) -
    Relationship between the values returned by keyboard-reading
    syscalls and physical keys.
-   [File_System]({{< ref "OS_Information/File_System.md" >}}) - Information on the
    main and storage memories.
-   [Timers]({{< ref "/OS_Information/Timers.md" >}}) - Information on the 10 software
    timers supported.
-   [Locale]({{< ref "OS_Information/Locale.md" >}}) - Information on the basic
    localization features of the OS.
    -   [Multi-byte_strings]({{< ref "Multi-byte_strings.md" >}}) - How
        some special characters are encoded.
-   [Fonts]({{< ref "Fonts.md" >}}) - Fonts included in the OS and
    characters supported by each.
-   [Setup]({{< ref "Setup.md" >}}) - Details on the settings system
    used for SET UP (Shift+Menu) settings: trigonometric mode, complex
    numbers mode, input and display modes, etc.
-   [Processes]({{< ref "Processes.md" >}}) - Information on the
    two-process architecture of the OS.
-   [Versions_and_versioning]({{< ref "Versions_and_versioning.md" >}}) -
    OS, [bootloader]({{< ref "CASIOABS.md" >}}) and official add-in
    versions, plus information on the versioning scheme.
-   [Bitmaps]({{< ref "OS_Information/Bitmaps.md" >}}) - Bitmaps contained in the OS -
    icons, [fonts]({{< ref "OS_Information/Fonts.md" >}}), function key labels,
    test/demo images, etc.
    -   [FKey_Bitmaps]({{< ref "OS_Information/FKey_Bitmaps.md" >}}) - OS-provided
        labels for the function key actions. You should use these in
        your add-ins instead of your own where possible, to maintain
        coherence with the OS and save space.
-   [Error_handling]({{< ref "Error_handling.md" >}}) - How the OS
    behaves in case of software and hardware error.

## Tutorials

*Article creation is still being completed. Please discuss this
section.*

### Intro to Prizm C Programming {#intro_to_prizm_c_programming}

1.  [PrizmSDK_Setup_Guide]({{< ref "Tutorials/PrizmSDK_Setup_Guide.md" >}}) -
    Setting up the PrizmSDK
2.  [Learning_the_Ropes]({{< ref "Tutorials/Learning_the_Ropes.md" >}}) -
    Learning how the SDK works and how to make projects
3.  [Hello_World]({{< ref "Tutorials/Hello_World.md" >}}) - Making your
    first Addin
4.  [Printing_Text]({{< ref "Tutorials/Printing_Text.md" >}}) - Shows
    different methods of printing strings and numbers.
5.  [Reading_Input]({{< ref "Tutorials/Reading_Input.md" >}}) - Getting
    user input
6.  [File Access]({{< ref "Tutorials/Using_the_File_System.md" >}}) -
    Managing files in the RAM and ROM
7.  [Popup windows](Popup_windows) - Shows how to use popup windows
8.  [Advances GUIs](Advances_GUIs) - Shows how to make advanced user
    inferfaces
9.  [Drawing Sprites](Drawing_Sprites) - Drawing pictures and shapes

-   [Debugging_Crashes]({{< ref "Debugging_Crashes.md" >}}) - Finding
    where your inevitable errors are.

### Zeldaking/Ashbad Tutorials {#zeldakingashbad_tutorials}

1.  [Introduction]({{< ref "Tutorials/Zeldaking_Tutorial_1.md" >}}) -
    Getting resources for learning C and setting up the SDK
2.  [The Basics]({{< ref "Tutorials/Zeldaking_Tutorial_2.md" >}}) -
    Learning how add-ins are made

## Discussion Forums {#discussion_forums}

-   [Casio Prizm Development and
    Programming](http://www.cemetech.net/forum/viewforum.php?f=68) at
    Cemetech
